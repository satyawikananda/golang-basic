package main

import "fmt"

func main() {
	// manifest typing => dimana cara penulisannya itu seperti var [nama var] [type data]
	var firstname string = "Satya"

	// type inference => dimana cara penulisannya itu tidak diawali dengan keyword var dan tidak menuliskan type data, dan type datanya itu mengikuti dari type data valuenya
	lastname := "wikananda"

	// multiple variable
	namaDepan, namaBelakang := "rena", "ayuni"

	// underscore variable => karena go memiliki aturan untuk menggunakan variable yang sudah di deklarasikan itu harus dipakai, jadi guna nya underscore variable ini adalah untuk menampung nilai yang tidak dipakao=i
	_ = "I learn golang"
	golang, _ := "Golang is best", "test test"

	// keyword new in variable => digunakan untuk membuat variable yang memiliki pointer
	name := new(string)

	fmt.Printf("hallo satya !\n")
	fmt.Printf("hello %s %s %s!\n", firstname, lastname, "test")
	fmt.Println("Hallo", firstname, lastname+"!")
	fmt.Println(namaDepan, namaBelakang)
	fmt.Println(golang)
	fmt.Println(name)
}
