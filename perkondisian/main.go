package main

import (
	"fmt"
)

func main() {
	// poin := 6

	// if poin == 10 {
	// 	fmt.Println("Wow nilai kamu sempurna")
	// } else if poin > 6 && poin <= 9 {
	// 	fmt.Println("Kerja bagus")
	// } else {
	// 	fmt.Println("Noob lo")
	// }

	// Variabel temporary di if else
	poin := 1112.0

	if percent := poin / 100; percent >= 100 {
		fmt.Printf("Hasil: %.1f%s Sempurna\n", percent, "%")
	} else if percent >= 70 {
		fmt.Printf("%.1f%s good\n", percent, "%")
	} else {
		fmt.Printf("%.1f%s not bad\n", percent, "%")
	}

	// switch case in golang
	nilai := 5

	switch {
	case nilai == 9:
		fmt.Println("Amazing")
	case (nilai < 6) && (nilai > 3):
		fmt.Println("Nice")
		fallthrough
	case nilai <= 5:
		fmt.Println("Belajar lagi ya")
	default:
		{
			fmt.Println("Noob")
			fmt.Println("Ganbatte")
		}
	}

	// Seleksi kondisi bersarang
	point := 8

	if point >= 8 {
		switch point {
		case 10:
			fmt.Println("Amazing")
		default:
			fmt.Println("Nice")
		}
	} else {
		if point <= 7 {
			fmt.Println("Ayo belajar lagi")
		} else if point == 4 {
			fmt.Println("keep trying")
		} else {
			fmt.Println("Goblok")
			if point == 0 {
				fmt.Println("TK aja lagi >:(")
			}
		}
	}
}
