package main

import (
	"fmt"
)

func main() {
	// tipe data numerik non desimal
	var positiveNumber uint8 = 10
	var negativeNumber int8 = -2

	fmt.Printf("Hasil bilangan: %d\n", positiveNumber)
	fmt.Printf("Hasil bilangan: %d\n", negativeNumber)
	// ------------------------------------------------

	// tipe data numerik desimal
	var desimal = 2.62
	fmt.Printf("Hasil bilangan: %.1f\n", desimal)
	// ------------------------------------------------

	// tipe data boolean
	exist := true
	fmt.Printf("Exist: %t\n", exist)
	// ------------------------------------------------
	// tipe data string
	fullname := "Satya wikananda"
	fmt.Println(fullname)

	message := `Hey I learn golang.
	Wish me luck :)`

	fmt.Println(message)
}
