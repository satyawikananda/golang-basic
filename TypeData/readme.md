## Cakupan nilai setiap tipe data non desimal

| Tipe data| Cakupan bilangan |
|----------|:-------------:|
| ```uint8```    |  0 <-> 255 |
| ```uint16``` |    0 <-> 65535   |
| ```uint32``` | 0 <-> 4294967295 |
| ```uint64``` | 0 <-> 18446744073709551615| 
| ```uint``` | Sama seperti unit32 atau uint64 (ini tergantung dengan nilainya)  | 
| ```uint64``` | 0 <-> 18446744073709551615| 
| ```byte``` | Sama dengan uint8 | 
| ```int8``` | -128 <-> 127 | 
| ```int16``` | -32768 <-> 32767 | 
| ```int32``` | -2147483648 <-> 21474836487 | 
| ```int64``` | -9223372036854775808 <-> 9223372036854775807 | 
| ```int``` | Sama dengan int32 atau int64 | 
| ```rune``` | Sama dengan int8 | 