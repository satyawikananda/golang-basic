package main

import (
	"fmt"
)

func main() {
	value := (((2+6)%3)*4 - 2) / 3
	isEqual := (value == 9)

	left := false
	right := true

	leftAndRight := left && right
	leftOrRight := left || right
	leftReserve := !left

	fmt.Printf("Hasil: %d\n", value)
	fmt.Printf("Nilai %d (%t) \n", value, isEqual)
	fmt.Printf("left && right is: %t \n", leftAndRight)
	fmt.Printf("left || right is: %t \n", leftOrRight)
	fmt.Printf("reverse left is: %t \n", leftReserve)
}
